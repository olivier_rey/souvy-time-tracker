<?php

/**
 * Created by PhpStorm.
 * User: chico
 * Date: 30/08/18
 * Time: 23:01
 */

namespace App\Controller;

use App\Entity\Task;
use App\Repository\TaskRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
class TaskController extends ApiController
{
    /**
     * @Route("/tasks")
     */
    public function index(TaskRepository $taskRepository)
    {
        $tasks = $taskRepository->transformAll();

        return $this->respond($tasks);
    }
}